class Http {
    #baseUrl = "https://0020-103-176-135-84.in.ngrok.io";
    #token1="";
    async send(endpoint, options = {}, data = null) {
        try {
            console.log(this.#token1);
            options = { 
                ...options,
                headers: {
                    "Authorization":"Bearer "+this.#token1,
                    "ngrok-skip-browser-warning": "1234",
                     'Content-type': 'application/json',
                    },
                body: data ? JSON.stringify(data) : null  
            }
            const response = await fetch(`${this.#baseUrl}/${endpoint}`, options);
            const parsedData = await response.json();
            console.log(parsedData);
            return parsedData;
        }
         catch (e) {
            console.log(e);
        }
    }

    async get(endpoint) {
        const entity= await this.send(endpoint);
        console.log(entity);
        return entity;
    }

    async delete(endpoint) {
        return await this.send(endpoint, { method: 'DELETE' });
    }

    async post(endpoint, data) {
        console.log("loinneddd")
        const ans=await this.send(endpoint, { method: 'POST' }, data);
        console.log(ans);
        if(ans.jwtToken)
        {
           this.#token1=ans.jwtToken;
        }
        return ans;
    } 
    
    async put(endpoint, data) {
        console.log(data);
        return await this.send(endpoint, { method: 'PUT'}, data)
    } 

}

const http = new Http();

export default http;
