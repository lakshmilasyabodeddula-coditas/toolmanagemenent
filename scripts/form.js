import {$} from "./references.js";
import http from "./http.js";
import adminPage from "./adminPage.js";
import workerSection from "./workerDashboard.js";


const createLoginForm=()=>{
        const loginFormTemplate=`<form>
                                    <label for="username">User Name</label>
                                    <input type="text" id="username"/>
                                    <label for="passowrd">Password</label>
                                    <input type="password" id="password">
                                    <input type="submit" value="submit" id="form-submit-btn">
                                </form>`
        $(".form-holder")[0].innerHTML=loginFormTemplate;
        $("#form-submit-btn").addEventListener("click",authenticateForm);
 }

 const authenticateForm=async (event)=>{
    event.preventDefault();
    const data={
        username:$("#username").value,
        password:$("#password").value
    }
    const authenticatedResponse= await http.post("authenticate",data);
    let role=authenticatedResponse.userRole;

    if(role==="ADMIN")
    {
      $(".form-holder")[0].style.display="none";
      const adminDashboard=adminPage['openAdminDashboard']();
     }
     else if(role==="WORKER")
     {
      const workerDashboard=workerSection['getAndDisplayOrders']();
     }
 }


 export default {
    authenticateForm,
    createLoginForm
 }