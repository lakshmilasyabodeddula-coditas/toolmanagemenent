import http from "./http.js";
import {$} from "./references.js";
import modalFactory from"./modalFactory.js";
import adminPage from "./adminPage.js";

let workerFormTemplate=`<form>  
                            <label for="workerName">workerName</label>
                            <input type="text" id="workerName">
                            <label for="workerUsername">workerUsername</label>
                            <input type="text" id="workerUsername">
                            <label for="workerPassword">workerPassword</label>
                            <input type="password" id="workerPassword">
                            <label for="workerSalary">workerSalary</label>
                            <input type="number" id="workerSalary">
                            <label for="worker_orders">worker Order</label>
                            <input type="text" id="worker_orders">
                            <input type="submit" id="submit-worker">
                        </form>`

let workerTableTemplate=`<table id="worker-table">
                          <thead>
                            <th>workerId</th>
                            <th>workerName</th>
                            <th>workerUsername</th>
                            <th>workerSalary</th>
                            <th>workerOrders</th>
                           </thead>
                        <tbody id="worker-table-body">
                        <td class="elementrow"></td>
                        </tbody>
                    </table>`

 const deleteTemplate=`<p>Are you sure you want to delete</p>
                    <button id="yesButton">Yes</button>
                     <button id="noButton">No</button`

const updateWorkerTemplate=`<form>
                                <label for="workerName">workerName</label>
                                <input type="text" id="workerName">
                                <label for="workerUsername">workerUsername</label>
                                <input type="text" id="workerUsername">
                                <label for="workerSalary">workerSalary</label>
                                <input type="number" id="workerSalary">
                                <label for="workerPassword">workerPassword</label>
                                <input type="password" id="workerPassword">
                                <input type="submit" id="update-submit-worker">
                            </form>`

async function displayWorkersTable()
{
    const workercreatebutton=document.createElement("button");
    workercreatebutton.textContent="create";

   $("#display-content").innerHTML=workerTableTemplate;
   $("#display-content").append(workercreatebutton);
   workercreatebutton.addEventListener("click",createWorker);
  const workers=await http['get']("admin/getWorkers");
   console.log(workers);
   let row=``
   for(let worker of workers)
   {
     row=row+workerRow(worker);
   }
   $("#worker-table-body").innerHTML=row;

   $("#worker-table").addEventListener("click",(event)=>{
    const selectedClass=event.target.className;
    console.log(selectedClass);
    if(selectedClass==="delete")
    {
        deleteRecord(event);
    }
    else if(selectedClass==="edit")
    {
        editWorker(event);
    }

})
}

const workerRow=(worker)=>{
    return (    
   `<tr>
    <td>${worker.workerId}</td>
    <td>${worker.workerName}</td>
    <td>${worker.workerUsername}</td>
    <td>${worker.workerSalary}</td>
    <td>${worker.worker_orders[0]}</td>
    <td><button id="${worker.workerId}" class="edit" class="btn">Edit</button></td>
    <td><button  id="${worker.workerId}" class="delete" class="btn">Delete</button></td>
     </tr>`);

}

const createWorker=async (event)=>
{
    console.log("create button clicked");
     event.preventDefault();
     const createModal=modalFactory['createModalFactory']();
     const createWorkerForm=createModal(workerFormTemplate);
     $("#submit-worker").addEventListener("click",async (event)=>{

        event.preventDefault();
        try{
        let formData={
            workerName:$("#workerName").value,
            workerUsername:$("#workerUsername").value,
            workerPassword:$("#workerPassword").value,
            workerSalary:$("#workerSalary").value,
        }
         console.log(formData);
         const postDetails=await http['post']("admin/createWorker",formData);

         }catch(e)
         {
            console.log(e);
         }
     }
     )
}

 const editWorker=async(event)=>{
    console.log("edit worker entered");
    let id=event.target.id;
    event.preventDefault();

    const createModal=modalFactory['createModalFactory']();
     const createWorkerForm=createModal(updateWorkerTemplate);

     $("#update-submit-worker").addEventListener("click",async (event)=>{
        event.preventDefault();
        try{
        let formData={
            workerId:id,
            workerName:$("#workerName").value,
            workerUsername:$("#workerUsername").value,
            workerSalary:$("#workerSalary").value,
            workerPassword:$("#workerPassword").value,
        }
         console.log(formData);
         const postDetails=await http.put("admin/updateWorker",formData);
         console.log(postDetails);

         }catch(e)
         {
            console.log(e);
         }
     }
     )
     displayWorkersTable();
}
function deleteRecord(event){
    event.preventDefault();
    let id=event.target.id;
    const createModal=modalFactory['createModalFactory']();
    const confirmDelete=createModal(deleteTemplate);
    $("#yesButton").addEventListener("click",(event)=>{
        event.preventDefault();
        http.delete(`admin/deleteWorker/${id}`)
        adminPage['openAdminDashboard']();
    })
    $("#noButton").addEventListener("click",()=>{
         confirmDelete.style.display="none";
    })
}

export default{
    displayWorkersTable
}