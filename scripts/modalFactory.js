import {$} from "./references.js";



const createModalFactory=()=>{
    
    return(templateForm)=>{
        const modalholder=$("#modal-holder");
         const modalContainer=document.createElement("div");
         modalContainer.classList.add("modal-container");
         const modal=document.createElement("div");   
         modal.classList.add("modal");
         modalContainer.appendChild(modal);
         modalholder.append(modalContainer);
         const display=document.getElementsByClassName("modal-container")[0];
         display.style.display="flex";
         display.innerHTML=templateForm;
         return display;
    };
 } 



 export default{
    createModalFactory
 }