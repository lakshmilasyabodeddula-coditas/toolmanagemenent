import { $ } from "./references.js";
import http from "./http.js";

const orderTableTemplate=`<table>
                                <thead>
                                    <th>Order id</th>
                                    <th>customer name</th>
                                    <th>worker id</th>
                                    <th>worker name</th>
                                    <th>ordered Tools</th>
                                    <th>order status</th>
                                    <th>price</th>
                                </thead>
                                <tbody id="order-table-body">
                                </tbody>
                            </table>`
                                     
const orderTable=async ()=>{
    $("#display-content").innerHTML=orderTableTemplate;
    const orders=await http.get("admin/getOrders");

    let row=``;
    console.log(orders);
    for(let order of orders)
    {
     row=row+orderRow(order);
    }
    $("#order-table-body").innerHTML=row;
}
const orderRow=(order)=>{
    return `<tr>
                <td>${order.orderId}</td>
                <td>${order.customerName}</td>
                <td>${order.workerId}</td>
                <td>${order.workerName}</td>
                <td>${order.worker_orders}</td>
                <td>${order.orderStatus}</td>
                <td>${order.price}</td>
            </tr>`
}

export default{
    orderTable
}