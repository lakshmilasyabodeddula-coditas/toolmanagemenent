import { $ } from "./references.js";
import workerspage from "./workerspage.js";
import orderspage from "./order.js"; 
import toolspage from "./tools.js";

function openAdminDashboard()
{    
    $("#display-container").style.display="flex";
    const adminDashBoardTemplate=`<section class="sidebar">
                                        <button id="worker-button" class="admin-control"><a> workers</a></button>
                                        <button id="order-button" class="admin-control"><a>orders</a></button>
                                        <button id="tool-button" class="admin-control"><a>tools</a></button>
                                  </section>`;
    $("#display-container").innerHTML=adminDashBoardTemplate;
    
    $("#worker-button").addEventListener("click",()=>{
        workerspage['displayWorkersTable']();
     })
      $("#order-button").addEventListener("click",()=>{
         orderspage['orderTable']();
      })
      $("#tool-button").addEventListener("click",()=>{
      toolspage['displayTools']();
      })
}


export default{
    openAdminDashboard
}