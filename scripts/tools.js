import { $ } from "./references.js";
import http from "./http.js";
import modalFactory from "./modalFactory.js";
const toolsTemplate=`<table id="tools-table">
                        <thead>
                            <th>toolId</th>
                            <th>toolName</th>
                            <th>toolSize</th>
                            <th>toolPrice</th>
                        </thead>
                        <tbody id="tools-body"></tbody>
                        </table>`

let toolTemplateForm=`<form>
                        <label for="toolName">Tool Name</label>
                        <input type="text" id="toolName">
                        <label for="toolSize">Tool Size</label>
                        <input type="number" id="toolSize">
                        <label for="toolPrice">Tool Price</label>
                        <input type="number" id="toolPrice">
                        <input type="button" id="submit">
                    </form>`


const deleteTemplate=`<p>Are you sure you want to delete</p>
                       <button id="yesButton">Yes</button>
                        <button id="noButton">No</button`


const displayTools=async ()=>{
    $("#display-content").innerHTML=toolsTemplate;

    const toolcreateButton=document.createElement("button");
    toolcreateButton.textContent="createTool";
    toolcreateButton.addEventListener("click",generateToolCreateForm);
    $("#display-content").append(toolcreateButton);

    const tools =await http['get']("admin/getTools");
    console.log(tools);

    let row=``
    for(let tool of tools) 
    {
       row=row+createToolRow(tool);
    }
    $("#tools-body").innerHTML=row;
    $("#tools-table").addEventListener("click",(event)=>{
        const selectedClass=event.target.className;
        console.log(selectedClass);
        if(selectedClass==="delete")
        {
            deleteRecord(event);
        }
        else if(selectedClass==="edit")
        {
            editTool(event);
        }
    });
}



function createToolRow(tool){
    return (`<tr>
       <td>${tool.toolId}</td>
       <td>${tool.toolName}</td>
       <td>${tool.toolSize}</td>
       <td>${tool.toolPrice}</td>
       <td><button id="${tool.toolId}" class="edit">Edit</button></td>
       <td><button id="${tool.toolId}" class="delete">Delete</button></td>       
   </tr>`);
}




const editTool=(event)=>{
    const selectedClass=event.target.className;
    console.log(selectedClass);

    const id=event.target.id;
    console.log(id);
    const createForm=modalFactory['createModalFactory']();
    const editToolForm=createForm(toolTemplateForm);

    $("#submit").addEventListener("click",()=>{
        const formData={
            toolName:$("#toolName").value,
            toolSize:$("#toolSize").value,
            toolPrice:$("#toolPrice").value
    }
    console.log(formData);
    http.put("admin/createTool",formData);
     }
    )
    displayTools();
}

const generateToolCreateForm =async (event)=>{
    event.preventDefault();
    const createForm=modalFactory['createModalFactory']();
    const createToolForm=createForm(toolTemplateForm);

         console.log(createToolForm);
    $("#submit").addEventListener("click",()=>{
        const formData={
            toolName:$("#toolName").value,
            toolSize:$("#toolSize").value,
            toolPrice:$("#toolPrice").value
    }
    console.log(formData);
    http.post("admin/createTool",formData);
     }
    )
    displayTools();
    }
   
const deleteRecord=(event)=>{
    
    console.log("hi")
    const selectedClass=event.target.className;
    console.log(selectedClass);

    const id=event.target.id;
    console.log(id);
    const createModal=modalFactory['createModalFactory']();
    const confirmDelete=createModal(deleteTemplate);
    $("#yesButton").addEventListener("click",()=>{
        http.delete(`admin/deleteTool/${id}`);
        displayTools();
    })
    $("#noButton").addEventListener("click",()=>{
        console.log("add delete no confirmation");
        displayTools();
    })
} 


export default{
    displayTools
}